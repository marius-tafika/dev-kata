package com.carrefour.backend_kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendKataApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackendKataApplication.class, args);
    }

}
