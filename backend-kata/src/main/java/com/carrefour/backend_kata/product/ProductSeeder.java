package com.carrefour.backend_kata.product;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class ProductSeeder implements CommandLineRunner {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public void run(String... args) throws Exception {
        List<Product> products = Arrays.asList(
                new Product("Wireless Mouse", new BigDecimal("45.00"), 12),
                new Product("Mechanical Keyboard", new BigDecimal("15.50"), 8),
                new Product("4K Monitor", new BigDecimal("23.75"), 20),
                new Product("Gaming Headset", new BigDecimal("99.99"), 3),
                new Product("USB-C Hub", new BigDecimal("5.00"), 50),
                new Product("External SSD", new BigDecimal("34.99"), 15),
                new Product("Bluetooth Speaker", new BigDecimal("75.00"), 7),
                new Product("Portable Charger", new BigDecimal("10.00"), 25),
                new Product("Smartphone Stand", new BigDecimal("49.99"), 30),
                new Product("Webcam", new BigDecimal("60.00"), 18),
                new Product("Wireless Earbuds", new BigDecimal("85.50"), 5),
                new Product("Laptop Stand", new BigDecimal("40.00"), 22),
                new Product("Ergonomic Mouse Pad", new BigDecimal("12.00"), 40),
                new Product("USB Flash Drive", new BigDecimal("27.50"), 14),
                new Product("Noise Cancelling Headphones", new BigDecimal("99.00"), 9)
        );

        // Optional: Clear existing products
        productRepository.deleteAll();

        // Save new products
        productRepository.saveAll(products);
    }
}
