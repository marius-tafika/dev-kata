package com.carrefour.backend_kata.user;

import java.util.ArrayList;
import java.util.List;

import com.carrefour.backend_kata.cart.Cart;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String email;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    private List<Cart> carts = new ArrayList<>();

    // Default constructor (required for JPA)
    public User() {
    }

    // Builder with name and email
    public User(String name, String email) {
        this.name = name;
        this.email = email;
    }

    // Getters et setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Cart> getCarts() {
        return carts;
    }

    public void setCarts(List<Cart> carts) {
        this.carts = carts;
    }

    // Helper method to add a cart
    public void addCart(Cart cart) {
        carts.add(cart);
        cart.setUser(this);
    }

    // Helper method to remove a cart
    public void removeCart(Cart cart) {
        carts.remove(cart);
        cart.setUser(null);
    }
}
