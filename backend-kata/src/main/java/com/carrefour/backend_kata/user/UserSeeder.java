package com.carrefour.backend_kata.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class UserSeeder implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {
        List<User> users = Arrays.asList(
            new User("Christian Dupont", "christian.dupont@example.com"),
            new User("Eric Martin", "eric.martin@example.com"),
            new User("Jeannot Lefebvre", "jeannot.lefebvre@example.com")
        );

        // Optional: Clear existing users
        userRepository.deleteAll();

        // Save new users
        userRepository.saveAll(users);
    }
}