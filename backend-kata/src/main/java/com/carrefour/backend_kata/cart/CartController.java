package com.carrefour.backend_kata.cart;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.carrefour.backend_kata.cart.cartItem.CartItem;
import com.carrefour.backend_kata.cart.cartItem.CartItemDto;
import com.carrefour.backend_kata.product.Product;
import com.carrefour.backend_kata.product.ProductRepository;
import com.carrefour.backend_kata.user.User;
import com.carrefour.backend_kata.user.UserRepository;
import com.carrefour.backend_kata.user.UserDto;

@RestController
@RequestMapping("/api/cart")
public class CartController {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/add")
    public ResponseEntity<CartDto> addToCart(@RequestParam Long userId, @RequestParam Long productId, @RequestParam int quantity) {
        User user = userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User not found"));
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));

        if (product.getStockQuantity() < quantity) {
            throw new RuntimeException("Not enough stock");
        }

        Cart cart = cartRepository.findByUserId(userId).orElseGet(() -> {
            Cart newCart = new Cart();
            newCart.setUser(user);
            newCart.setExpirationTime(LocalDateTime.now().plusHours(1));
            return newCart;
        });

        CartItem cartItem = cart.getItems().stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst()
                .orElseGet(() -> {
                    CartItem newItem = new CartItem();
                    newItem.setCart(cart);
                    newItem.setProduct(product);
                    cart.getItems().add(newItem);
                    return newItem;
                });

        cartItem.setQuantity(cartItem.getQuantity() + quantity);
        product.setStockQuantity(product.getStockQuantity() - quantity);

        productRepository.save(product);
        Cart savedCart = cartRepository.save(cart);

        return ResponseEntity.ok(convertToDto(savedCart));
    }

    @GetMapping("/{userId}")
    public ResponseEntity<CartDto> getCart(@PathVariable Long userId) {
        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new RuntimeException("Cart not found"));
        return ResponseEntity.ok(convertToDto(cart));
    }

    @PutMapping("/update")
    public ResponseEntity<CartDto> updateCartItem(@RequestParam Long userId, @RequestParam Long productId, @RequestParam int newQuantity) {
        Cart cart = cartRepository.findByUserId(userId).orElseThrow(() -> new RuntimeException("Cart not found"));
        Product product = productRepository.findById(productId).orElseThrow(() -> new RuntimeException("Product not found"));

        CartItem cartItem = cart.getItems().stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Product not in cart"));

        int quantityDifference = newQuantity - cartItem.getQuantity();
        if (product.getStockQuantity() < quantityDifference) {
            throw new RuntimeException("Not enough stock");
        }

        cartItem.setQuantity(newQuantity);
        product.setStockQuantity(product.getStockQuantity() - quantityDifference);

        productRepository.save(product);
        Cart updatedCart = cartRepository.save(cart);
        return ResponseEntity.ok(convertToDto(updatedCart));
    }

    @DeleteMapping("/removeItem")
    public ResponseEntity<CartDto> removeItemFromCart(@RequestParam Long userId, @RequestParam Long productId) {
        Cart cart = cartRepository.findByUserId(userId)
                .orElseThrow(() -> new RuntimeException("Cart not found"));

        CartItem cartItemToRemove = cart.getItems().stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Product not in cart"));

        cart.getItems().remove(cartItemToRemove);
        Cart updatedCart = cartRepository.save(cart);

        return ResponseEntity.ok(convertToDto(updatedCart));
    }

    @PutMapping("/updateQuantity")
    public ResponseEntity<CartDto> updateCartItemQuantity(@RequestParam Long userId, @RequestParam Long productId, @RequestParam int newQuantity) {
        Cart cart = cartRepository.findByUserId(userId)
                .orElseThrow(() -> new RuntimeException("Cart not found"));

        CartItem cartItemToUpdate = cart.getItems().stream()
                .filter(item -> item.getProduct().getId().equals(productId))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Product not in cart"));

        int oldQuantity = cartItemToUpdate.getQuantity();
        int quantityDifference = newQuantity - oldQuantity;

        if (quantityDifference > 0) {
            Product product = cartItemToUpdate.getProduct();
            if (product.getStockQuantity() < quantityDifference) {
                throw new RuntimeException("Not enough stock");
            }
            product.setStockQuantity(product.getStockQuantity() - quantityDifference);
            productRepository.save(product);
        } else if (quantityDifference < 0) {
            Product product = cartItemToUpdate.getProduct();
            product.setStockQuantity(product.getStockQuantity() - quantityDifference);
            productRepository.save(product);
        }

        cartItemToUpdate.setQuantity(newQuantity);
        Cart updatedCart = cartRepository.save(cart);

        return ResponseEntity.ok(convertToDto(updatedCart));
    }

    private CartDto convertToDto(Cart cart) {
        CartDto cartDto = new CartDto();
        cartDto.setId(cart.getId());
        cartDto.setExpirationTime(cart.getExpirationTime());

        UserDto userDto = new UserDto();
        userDto.setId(cart.getUser().getId());
        userDto.setName(cart.getUser().getName());
        userDto.setEmail(cart.getUser().getEmail());
        cartDto.setUser(userDto);

        for (CartItem item : cart.getItems()) {
            CartItemDto itemDto = new CartItemDto();
            itemDto.setId(item.getId());
            itemDto.setProductId(item.getProduct().getId());
            itemDto.setQuantity(item.getQuantity());
            cartDto.getItems().add(itemDto);
        }

        return cartDto;
    }
}
