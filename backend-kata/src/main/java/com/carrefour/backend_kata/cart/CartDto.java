package com.carrefour.backend_kata.cart;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.carrefour.backend_kata.cart.cartItem.CartItemDto;
import com.carrefour.backend_kata.user.UserDto;

public class CartDto {

    private Long id;

    private UserDto userDto;

    private List<CartItemDto> items = new ArrayList<>();

    private LocalDateTime expirationTime;

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getUser() {
        return userDto;
    }

    public void setUser(UserDto userDto) {
        this.userDto = userDto;
    }

    public List<CartItemDto> getItems() {
        return items;
    }

    public void setItems(List<CartItemDto> items) {
        this.items = items;
    }

    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(LocalDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }
}
