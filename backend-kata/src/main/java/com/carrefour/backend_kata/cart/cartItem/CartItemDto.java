package com.carrefour.backend_kata.cart.cartItem;

import com.carrefour.backend_kata.cart.CartDto;
import com.carrefour.backend_kata.product.ProductDto;

public class CartItemDto {

    private Long id;
    private CartDto cartDto;
    private ProductDto productDto;
    private int quantity;

    // Getters and setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public CartDto getCart() {
        return cartDto;
    }

    public void setCart(CartDto cartDto) {
        this.cartDto = cartDto;
    }

    public ProductDto getProduct() {
        return productDto;
    }

    public void setProduct(ProductDto productDto) {
        this.productDto = productDto;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setProductId(Long productId) {
        if (this.productDto == null) {
            this.productDto = new ProductDto();
        }
        this.productDto.setId(productId);
    }
}
