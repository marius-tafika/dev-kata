export type TProduct = {
  id: string;
  name: string;
  price: number;
  stockQuantity: number;
};

export type TUser = {
  id: number;
  name: string;
  email: string;
};

export type CartItem = {
  id: number;
  product: TProduct;
  quantity: number;
};

export type Cart = {
  id: number;
  user: TUser;
  items: CartItem[];
  expirationTime: string;
};

export type User = {
  id: number;
  name: string;
  email: string;
  carts: Cart[];
};
