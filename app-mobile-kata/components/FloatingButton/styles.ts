import { StyleSheet } from "react-native";

const useStyles = StyleSheet.create({
  fab: {
    position: "absolute",
    width: 56,
    height: 56,
    borderRadius: 28,
    backgroundColor: "#007BFF",
    justifyContent: "center",
    alignItems: "center",
    right: 16,
    bottom: 16,
    elevation: 8,
  },
});

export default useStyles;
