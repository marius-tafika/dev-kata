import React from "react";
import { Icon } from "@rneui/base";
import { TouchableOpacity, StyleProp, ViewStyle } from "react-native";
import styles from "./styles";

export interface IFloatingButtonProps {
  style?: StyleProp<ViewStyle>;
  onPress: () => void;
  iconName: string;
  iconType?: string;
}

const FloatingButton = (props: IFloatingButtonProps) => {
  const { style, onPress, iconName, iconType = "material" } = props;

  return (
    <TouchableOpacity style={[styles.fab, style]} onPress={onPress}>
      <Icon name={iconName} type={iconType} color="white" size={24} />
    </TouchableOpacity>
  );
};

export default FloatingButton;
