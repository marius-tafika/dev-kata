import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { Icon } from "@rneui/base";

import ProductsScreen from "screens/ProductsScreen";
import MyCartsScreen from "screens/MyCartsScreen";

const TabNavigator = () => {
  const Tab = createBottomTabNavigator();

  return (
    <Tab.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color, size }) => {
          let iconName = "";
          size = 24;

          if (route.name === "Products") {
            iconName = "list";
          } else if (route.name === "My cart") {
            iconName = "shopping-cart";
          }

          return (
            <Icon name={iconName} type="material" color={color} size={size} />
          );
        },
        tabBarActiveTintColor: "#007BFF",
        tabBarInactiveTintColor: "#808080",
        tabBarActiveBackgroundColor: "#D3D3D3",
        tabBarInactiveBackgroundColor: "#FFFFFF",
      })}
    >
      <Tab.Screen name="Products" component={ProductsScreen} />
      <Tab.Screen name="My cart" component={MyCartsScreen} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
