import { StyleSheet } from "react-native";

const useStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    padding: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 20,
  },
  inputContainer: {
    width: "100%",
    marginVertical: 10,
  },
  buttonContainer: {
    width: "100%",
    marginVertical: 10,
  },
  button: {
    backgroundColor: "#2196F3",
    borderRadius: 5,
  },
  buttonTitle: {
    color: "#fff",
    fontWeight: "bold",
  },
});

export default useStyles;
