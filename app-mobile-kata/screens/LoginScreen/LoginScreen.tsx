import React, { useState } from "react";
import { View, Text } from "react-native";
import { Button, Input } from "@rneui/base";

import { APP_NAME, USER_ID } from "constants/app";

import styles from "./styles";
import AsyncStorage from "@react-native-async-storage/async-storage";

export interface ILoginScreenProps {
  navigation: any; // Add this line to define the navigation prop
}

const LoginScreen = ({ navigation }: ILoginScreenProps) => {
  const [email, setEmail] = useState("christian.dupont@example.com");
  const [password, setPassword] = useState("password");

  const clearAsyncStorage = async () => {
    try {
      const existingUserId = await AsyncStorage.getItem(USER_ID);
      if (existingUserId) {
        console.log("Found user id in AsyncStorage:", existingUserId);
        await AsyncStorage.clear();
      }

      /*
       * This is the user ID stored in AsyncStorage at the moment.
       * Ideally, you should use a token, stored in the backend context.
       * For example, using Redis.
       */

      const userId = "XXX"; // By default: "1"
      await AsyncStorage.setItem(USER_ID, userId);
      navigation.navigate("Main");
    } catch (error) {
      console.error("Failed to clear AsyncStorage", error);
    }
  };

  const handleLogin = async () => {
    await clearAsyncStorage();
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{APP_NAME}</Text>
      <Input
        placeholder="Email"
        containerStyle={styles.inputContainer}
        value={email}
        onChangeText={setEmail}
        autoCapitalize="none"
      />
      <Input
        placeholder="Password"
        containerStyle={styles.inputContainer}
        value={password}
        onChangeText={setPassword}
        secureTextEntry
      />
      <Button
        title="Log In"
        containerStyle={styles.buttonContainer}
        buttonStyle={styles.button}
        titleStyle={styles.buttonTitle}
        onPress={handleLogin}
      />
    </View>
  );
};

export default LoginScreen;
