import React, { useEffect, useState, useCallback } from "react";
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  RefreshControl,
  TouchableOpacity,
  Alert,
} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import FloatingButton from "components/FloatingButton";
import ProductItem from "screens/ProductsScreen/ProductItem";

import { API_URL } from "config";
import { CartItem } from "types/allTypes";
import { CART_ID } from "constants/app";

import styles from "./styles";

const MyCartsScreen = () => {
  const [cartItems, setCartItems] = useState<CartItem[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [refreshing, setRefreshing] = useState<boolean>(false);

  const fetchCartItems = useCallback(async () => {
    setLoading(true);
    const cartId = await AsyncStorage.getItem(CART_ID);
    const url = `${API_URL}/api/cart-items/cart/${cartId}`;

    try {
      const response = await fetch(url);
      const data = await response.json();
      if (Array.isArray(data)) {
        setCartItems(data);
      } else {
        setCartItems([]);
      }
      setLoading(false);
      setRefreshing(false);
    } catch (error) {
      console.error("Error fetching cart:", error);
      setLoading(false);
      setRefreshing(false);
    }
  }, []);

  useEffect(() => {
    fetchCartItems();
  }, [fetchCartItems]);

  const onRefresh = () => {
    setRefreshing(true);
    fetchCartItems();
  };

  const renderItem = ({ item }: { item: CartItem }) => (
    <ProductItem
      cartItemId={item.id.toString()}
      product={{ ...item.product, stockQuantity: item.quantity }}
      callBack={onRefresh}
    />
  );

  // Calculating total prices
  const total = Array.isArray(cartItems)
    ? cartItems.reduce(
        (acc, curr) => acc + curr.product.price * curr.quantity,
        0
      )
    : 0;

  const handleBuy = () => {
    Alert.alert("Info", `You need to pay $${total.toFixed(2)}`, [
      {
        text: "Cancel",
        style: "cancel",
      },
      {
        text: "OK",
        onPress: () => {
          // TODO: purchase logic here
          console.log("Purchase confirmed");
        },
      },
    ]);
  };

  if (loading) {
    return (
      <View style={styles.loadingContainer}>
        <ActivityIndicator size="large" color="#007BFF" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {cartItems.length > 0 && (
        <View style={styles.header}>
          <Text style={styles.headerText}>Total: ${total.toFixed(2)}</Text>
          <TouchableOpacity style={styles.buyButton} onPress={handleBuy}>
            <Text style={styles.buyButtonText}>Buy</Text>
          </TouchableOpacity>
        </View>
      )}
      <FlatList
        data={cartItems}
        keyExtractor={(item) => item.id.toString()}
        renderItem={renderItem}
        ListEmptyComponent={
          <View style={styles.emptyComponent}>
            <Text style={styles.emptyText}>No items in the cart</Text>
          </View>
        }
        refreshControl={
          <RefreshControl
            refreshing={refreshing}
            onRefresh={onRefresh}
            colors={["#007BFF"]}
            tintColor="#007BFF"
          />
        }
      />
      <FloatingButton
        onPress={onRefresh}
        iconName="refresh"
        iconType="material"
      />
    </View>
  );
};

export default MyCartsScreen;
