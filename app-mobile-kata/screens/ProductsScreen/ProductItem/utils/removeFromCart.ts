import { API_URL } from "config";

export const removeFromCart = async (
  cartItemId: string,
  callBack?: () => void
) => {
  const url = `${API_URL}/api/cart-items/${cartItemId}`;

  try {
    const response = await fetch(url, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    callBack?.();
  } catch (error) {
    console.error("Error removing item from cart:", error);
    throw error;
  }
};
