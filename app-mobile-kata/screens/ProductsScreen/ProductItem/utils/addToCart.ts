import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_URL } from "config";
import { CART_ID, USER_ID } from "constants/app";
import { Cart } from "types/allTypes";

export const addToCart = async (productId: string, quantity: number) => {
  const userId = await AsyncStorage.getItem(USER_ID);

  const url = `${API_URL}/api/cart/add?userId=${userId}&productId=${productId}&quantity=${quantity}`;

  fetch(`${url}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      return response.json();
    })
    .then(async (data: Cart) => {
      const cartId = await AsyncStorage.getItem(CART_ID);
      if (cartId !== data.id.toString()) {
        //The best thing to do here is to define the current cart in the backend context
        await AsyncStorage.setItem(CART_ID, data.id.toString());
        return;
      }
    })
    .catch((error) => {
      console.error("Error adding to cart:", error);
    });
};
