import { API_URL } from "config";

export const updateCartItemQuantity = async (
  cartItemId: string,
  newQuantity: number,
  callBack?: () => void
) => {
  const url = `${API_URL}/api/cart-items/${cartItemId}?quantity=${newQuantity}`;

  try {
    const response = await fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response.ok) {
      throw new Error("Network response was not ok");
    }
    callBack?.();
  } catch (error) {
    console.error("Error on updating item from cart:", error);
    throw error;
  }
};
