import { useState } from "react";
import { Alert } from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";

import { CART_ID } from "constants/app";
import { addToCart } from "./addToCart"; // Changed import path

interface ICheckCartProps {
  productId: string;
  quantity: number;
}

export const cartChecking = () => {
  const [modalVisible, setModalVisible] = useState(false);

  const checkCart = async ({ productId, quantity }: ICheckCartProps) => {
    try {
      const cartId = await AsyncStorage.getItem(CART_ID);
      console.log({ cartId });
      if (!cartId) {
        Alert.alert(
          "New Cart",
          "Do you want to create a new cart?",
          [
            {
              text: "No",
              onPress: () => console.log("Cancelled"),
              style: "cancel",
            },
            { text: "Yes", onPress: () => addToCart(productId, quantity) },
          ],
          { cancelable: false }
        );
      } else {
        setModalVisible((oldValue) => !oldValue);
        addToCart(productId, quantity);
      }
    } catch (error) {
      Alert.alert(
        "Error",
        "There was an error checking the cart. Please try again later.",
        [
          {
            text: "OK",
            onPress: () => console.log("Error acknowledged"),
          },
        ],
        { cancelable: false }
      );
    }
  };

  return { modalVisible, setModalVisible, checkCart };
};
