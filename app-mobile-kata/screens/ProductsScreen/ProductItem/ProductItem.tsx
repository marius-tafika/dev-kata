import React, { useState } from "react";
import { View, Text, TouchableOpacity, Alert } from "react-native";
import { Icon } from "@rneui/base";
import { CartItem, TProduct } from "types/allTypes";

import { removeFromCart } from "./utils";
import AddToCartModal from "../AddToCartModal";

import styles from "./styles";

interface IProductItemProps {
  product: TProduct;
  cartItemId?: string;
  callBack?: () => void;
}

const ProductItem = ({
  product,
  cartItemId = "",
  callBack,
}: IProductItemProps) => {
  const { name, price, stockQuantity } = product;

  const [modalVisible, setModalVisible] = useState(false);

  const handleDelete = () => {
    Alert.alert("Confirmation", "Are you sure you want to delete this item?", [
      {
        text: "No",
        style: "cancel",
      },
      {
        text: "Yes",
        onPress: () => {
          removeFromCart(cartItemId, callBack);
        },
      },
    ]);
  };

  const formatPrice = (price: number | null | undefined) => {
    if (typeof price === "number") {
      return price.toFixed(2);
    }
    return "0.00";
  };

  return (
    <View style={styles.itemContainer}>
      <View style={styles.itemDetails}>
        <Text style={styles.itemName}>{name}</Text>
        <Text style={styles.itemPrice}>${formatPrice(price)}</Text>
        <Text style={styles.itemStock}>
          {!!cartItemId
            ? `Quantity: ${stockQuantity}`
            : `Stock: ${stockQuantity}`}
        </Text>
      </View>
      {!!cartItemId ? (
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => setModalVisible(true)}
            style={[styles.iconButton, { backgroundColor: "#007BFF" }]}
          >
            <Icon name="edit" type="material" color="white" size={24} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={handleDelete}
            style={[styles.iconButton, { backgroundColor: "red" }]}
          >
            <Icon name="delete" type="material" color="white" size={24} />
          </TouchableOpacity>
        </View>
      ) : (
        <TouchableOpacity
          onPress={() => setModalVisible(true)}
          style={[styles.iconButton, { backgroundColor: "#007BFF" }]}
        >
          <Icon
            name="cart-plus"
            type="material-community"
            color="white"
            size={24}
          />
        </TouchableOpacity>
      )}
      {modalVisible && (
        <AddToCartModal
          product={product}
          cartItemId={cartItemId}
          initialQuantity={!!cartItemId ? stockQuantity : undefined}
          handleCloseModal={() => {
            setModalVisible(false);
          }}
          callBack={callBack}
        />
      )}
    </View>
  );
};

export default ProductItem;
