import { StyleSheet, Dimensions } from "react-native";

const { width } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#f5f5f5",
  },

  title: {
    fontSize: 24,
    fontWeight: "bold",
    marginBottom: 16,
  },

  itemContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 16,
    marginVertical: 8,
    backgroundColor: "#fff",
    borderRadius: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.1,
    shadowRadius: 4,
    elevation: 2,
    width: width - 32, // Full width minus container padding
  },

  itemName: {
    fontSize: 18,
    fontWeight: "bold",
  },

  itemPrice: {
    fontSize: 16,
    color: "#888",
  },

  itemStock: {
    fontSize: 14,
    color: "#444",
  },

  itemDetails: {
    flex: 1,
  },

  iconButton: {
    backgroundColor: "#007BFF",
    borderRadius: 8,
    padding: 8,
    marginLeft: 8,
  },

  buttonContainer: {
    flexDirection: "row",
  },
});

export default styles;
