import React, { useState } from "react";
import { View, Text, Modal, TextInput } from "react-native";
import { Button } from "@rneui/base";

import { TProduct } from "types/allTypes";
import { cartChecking } from "../ProductItem/utils";
import styles from "./styles";
import { updateCartItemQuantity } from "../ProductItem/utils/updateCartItemQuantity";

export interface IAddToCartModalProps {
  product: TProduct;
  initialQuantity?: number;
  cartItemId?: string;

  handleCloseModal: () => void;
  callBack?: () => void;
}

const AddToCartModal = (props: IAddToCartModalProps) => {
  const {
    product,
    initialQuantity,
    cartItemId = "",
    handleCloseModal,
    callBack,
  } = props;

  const [quantityValue, setQuantityValue] = useState(
    initialQuantity ? initialQuantity.toString() : "1"
  );
  const { checkCart } = cartChecking();

  const { id: productId, name, stockQuantity } = product;

  const handleAddToCart = (cartItemId?: string) => {
    if (!cartItemId) {
      const quantity = parseInt(quantityValue, 10);
      if (isNaN(quantity) || quantity <= 0) {
        alert("Please enter a valid quantity.");
        return;
      }
      if (quantity > stockQuantity) {
        alert(`Sorry, only ${stockQuantity} items are available in stock.`);
        return;
      }

      handleCloseModal();
      checkCart({ productId, quantity });
    } else {
      updateCartItemQuantity(cartItemId, parseInt(quantityValue, 10), () => {
        handleCloseModal();
        callBack?.();
      });
    }
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={true}
      onRequestClose={handleCloseModal}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Text style={styles.modalTitle}>{name}</Text>
          <Text style={styles.modalSubtitle}>
            Maximum quantity: {stockQuantity}
          </Text>
          <TextInput
            style={styles.input}
            onChangeText={setQuantityValue}
            value={quantityValue}
            placeholder="Quantity"
            keyboardType="numeric"
            textAlign="right"
          />
          <View style={styles.buttonContainer}>
            <Button onPress={handleCloseModal}>
              <Text style={styles.cancelButtonText}>Cancel</Text>
            </Button>
            <Button onPress={() => handleAddToCart(cartItemId)}>
              <Text style={styles.addButtonText}>
                {initialQuantity ? "Update" : "Add"}
              </Text>
            </Button>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default AddToCartModal;
