import { StyleSheet, Dimensions } from "react-native";

const { width, height } = Dimensions.get("window");

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    backgroundColor: "#f5f5f5",
  },

  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: height,
    width: width,
  },

  emptyComponent: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    height: height - 32, // Subtracting container padding
  },

  emptyText: {
    fontSize: 18,
    color: "#888",
  },
});

export default styles;
