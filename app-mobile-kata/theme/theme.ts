import { createTheme } from "@rneui/themed";

const theme = createTheme({
  mode: "light", // Set the initial mode to 'light' or 'dark'
  lightColors: {
    primary: "#FF0000",
    secondary: "#00FF00",
    background: "#FFFFFF",
  },
  darkColors: {
    primary: "#0000FF",
    secondary: "#FFFF00",
    background: "#000000",
  },
  components: {
    Button: {
      raised: true,
      buttonStyle: {
        backgroundColor: "#FF5733", // Custom button color
      },
      titleStyle: {
        color: "#FFFFFF", // Text color for the button
      },
    },
    Input: {
      inputStyle: {
        color: "#000000", // Text color for input
      },
      placeholderTextColor: "#888888",
    },
    Header: {
      backgroundColor: "#333333", // Header background color
    },
  },
});

export default theme;
