import Config from "react-native-config";

// Use terminal and type: ipconfig getifaddr en0 for getting local IP
const MY_IP = "192.168.1.184";
export const API_URL = `http://${MY_IP}:8080`;
