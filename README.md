# Kata Project

## Prerequisites

Before starting the project, make sure you have the following installed on your machines:

- [Docker](https://www.docker.com/get-started)
- [Expo CLI](https://docs.expo.dev/get-started/installation/)

## Starting the Backend

To start the backend, follow these steps:

1. Open a terminal.
2. Navigate to the `backend-kata` folder:
   ```bash
   cd backend-kata
   ```
3. Run the following command to build and start the Docker containers:
   ```bash
   docker-compose up -d --build
   ```

## Starting the Frontend

To start the mobile application, follow these steps:

1. Open a terminal.
2. Navigate to the `app-mobile-kata` folder:
   ```bash
   cd app-mobile-kata
   ```
3. Install the necessary dependencies with Yarn:
   ```bash
   yarn
   ```
4. Start the application with Expo:
   ```bash
   yarn start
   ```
5. About the user:
   The database is exposed on [::]:5436
   In the users table, we take an ID

Add it here: app-mobile-kata/screens/LoginScreen/LoginScreen.tsx

```bash
    /*
    * This is the user ID stored in AsyncStorage at the moment.
    * Ideally, you should use a token, stored in the backend context.
    * For example, using Redis.
    */

   const userId = "XXX";
   await AsyncStorage.setItem(USER_ID, userId);
   navigation.navigate("Main");
```

## Notes

- Make sure Docker is running before executing the commands.
- You can access the mobile application via the Expo Go app on your mobile device or using an emulator.

Happy coding 🚀!
